require 'spec_helper'

describe "cards/index" do
  before(:each) do
    assign(:cards, [
      stub_model(Card,
        :name => "Name",
        :thought_value => 1,
        :shape => "Shape",
        :size => "Size"
      ),
      stub_model(Card,
        :name => "Name",
        :thought_value => 1,
        :shape => "Shape",
        :size => "Size"
      )
    ])
  end

  it "renders a list of cards" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Shape".to_s, :count => 2
    assert_select "tr>td", :text => "Size".to_s, :count => 2
  end
end
