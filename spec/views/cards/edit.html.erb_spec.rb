require 'spec_helper'

describe "cards/edit" do
  before(:each) do
    @card = assign(:card, stub_model(Card,
      :name => "MyString",
      :thought_value => 1,
      :shape => "MyString",
      :size => "MyString"
    ))
  end

  it "renders the edit card form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => cards_path(@card), :method => "post" do
      assert_select "input#card_name", :name => "card[name]"
      assert_select "input#card_thought_value", :name => "card[thought_value]"
      assert_select "input#card_shape", :name => "card[shape]"
      assert_select "input#card_size", :name => "card[size]"
    end
  end
end
