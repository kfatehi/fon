# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Card.create({
 name: "Midget",
 thought_value: 40,
 shape: "circle",
 size: "2",
 })

Card.create({
 name: "Behemoth",
 thought_value: 400,
 shape: "circle",
 size: "40",
 })

Card.create({
 name: "Deci-Midget",
 thought_value: 40,
 shape: "circle",
 size: "1.2",
 })

Card.create({
 name: "The Wall",
 thought_value: 120,
 shape: "rectangle",
 size: "100x10",
 })

Card.create({
 name: "Deci-Wall",
 thought_value: 120,
 shape: "rectangle",
 size: "100.5x1.5",
 })