class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
      t.string :name
      t.integer :thought_value
      t.string :shape
      t.string :size

      t.timestamps
    end
  end
end
