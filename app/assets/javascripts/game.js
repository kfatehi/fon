var player_thought_generation_interval_id = false;

function pop_random_mind_card() {
  var index = Math.floor((Math.random()*(mind_cards.length-1)));
  
  var card = mind_cards[index];
  mind_cards.splice(index, 1);  
  
  return card;
}

function move_to_vision(card) {
  vision_cards.push(card);
  var card_element = $('#mind .cards #mind_card_'+card.id);
  $('#vision .cards').append(card_element.clone());
  card_element.remove();
}

function move_to_hand(card) {
  hand_cards.push(card);
  var card_element = $('#vision .cards #mind_card_'+card.id);
  $('#player .hand').append(card_element.clone());
  card_element.remove();
}

function generate_thought() {
  var gen_rate = $('input#thought_generation_rate').val();
  console.log('generate '+gen_rate+' thought');
  var thought_gen_field = $('input#thought_value');
  var thought_value = parseInt(thought_gen_field.val(), 10);
  current_thought_value = thought_value+parseInt(gen_rate, 10)
  thought_gen_field.val(current_thought_value);
  // --------------
  console.log('vision cards are currently:');
  console.log(vision_cards);
  if (vision_cards.length > 0) {
    if (current_thought_value >= vision_cards[0].thought_value) {
      card = vision_cards.pop();
      thought_gen_field.val(current_thought_value-card.thought_value);
      move_to_hand(card);
      if (mind_cards.length > 0)
        move_to_vision(pop_random_mind_card());
    }
  }
}

function init() {
  console.log('Initialized.');
  $('#generate_thought').click(function (){
    if (player_thought_generation_interval_id)
      clearInterval(player_thought_generation_interval_id);
    player_thought_generation_interval_id = setInterval(generate_thought, 1000);
    move_to_vision(pop_random_mind_card());
    $(this).attr('disabled', 'disabled');
  });
}

